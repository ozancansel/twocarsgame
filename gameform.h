#ifndef GAMEFORM_H
#define GAMEFORM_H

#include <QWidget>
#include <QLabel>
#include <QHash>
#include "gameapi.h"

namespace Ui {
class GameForm;
}

class GameForm : public QWidget
{
    Q_OBJECT

public:

    explicit GameForm(QWidget *parent = 0);
    ~GameForm();

    typedef struct{
        QString     player;
        QLabel*     nameLabel;
        QLabel*     readyLabe;
    } LobbyControls;

public slots:

    void    setName(QString &name);
    void    connectGameServer(QString &url);
    void    players(QStringList &players);

private:

    Ui::GameForm *ui;
    GameApi         _api;
    QString         _userName;
    LobbyControls   opponent;

};

#endif // GAMEFORM_H
