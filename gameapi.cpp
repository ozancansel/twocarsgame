#include "gameapi.h"
#include "message.h"

GameApi::GameApi(QObject *parent) : QObject(parent) {
    startTimer(16);
    connect(&_socket , SIGNAL(textMessageReceived(QString)) , this , SLOT(textMessageReceived(QString)));

    _gameStateToStringMap[WAITING_EVERYONE_TO_CONNECT_STATE] = WAITING_EVERYONE_TO_CONNECT;
    _gameStateToStringMap[LOBBY_NOT_READY_STATE] = LOBBY_NOT_READY;
    _gameStateToStringMap[GAME_STARTING_STATE] = GAME_STARTING;
    _gameStateToStringMap[GAME_STARTED_STATE] = GAME_STARTED;
}

void GameApi::connected(){
    if(debugEnabled()) qDebug() <<  "GameApi -> Oyuna baglanildi.";
}

void GameApi::textMessageReceived(QString message){
    Message msg(message);

    if(debugEnabled()) qDebug() << "Mesaj alindi -> " << message;

    if(msg.code() == GAME_STATE_CHANGED_COMMAND){
        changeState(_gameStateToStringMap[msg.args().at(0)]);
    } else if(msg.code() == PLAYER_INFORMATIONS_COMMAND){
        QStringList args = msg.args();
        emit players(args);
    }
}

void GameApi::error(QAbstractSocket::SocketError error){

}

void GameApi::connectToGame(QString &url){
    _socket.open(url);
}

void GameApi::setDebugEnabled(bool enabled){
    _debugEnabled = enabled;
}

bool GameApi::debugEnabled(){
    return _debugEnabled;
}

void GameApi::timerEvent(QTimerEvent *event){

    //Eger baglanilmadiysa geri donuluyor
    if(_socket.state() != QAbstractSocket::ConnectedState)
        return;

    QString command;
    while(!_commandQueue.isEmpty()){
        QString     queuedCmd = _commandQueue.dequeue();
        command.append(QString("%0|").arg(queuedCmd));

        if(command.length() > 2000){
            command.remove(command.length() - 1 , 1);
            _socket.sendTextMessage(command);

            if(debugEnabled())  qDebug() << "GameApi Komut gonderiliyor -> " << command;
        }
    }

    if(!command.isEmpty()){
        command.remove(command.length() - 1 , 1);
        _socket.sendTextMessage(command);

        if(debugEnabled()) qDebug() << "GameApi Komut gonderiliyor -> "<< command;
    }
}

void GameApi::login(QString &name){
    Message msg;
    QString code(SET_NAME);
    msg.setCode(code);
    msg.setArgs(QStringList() << name);
    _commandQueue.enqueue(msg.serialize());
}

void GameApi::changeState(GameState state){
    _state = state;
}
