#include "serverapi.h"
#include "message.h"

ServerApi::ServerApi(QObject *parent) : QObject(parent)
{
    connect(&_socket , SIGNAL(connected()) , this , SLOT(connected()));
    connect(&_socket , SIGNAL(stateChanged(QAbstractSocket::SocketState)) , this , SLOT(stateChanged(QAbstractSocket::SocketState)));
    connect(&_socket , SIGNAL(textMessageReceived(QString)) , this , SLOT(textMessageReceived(QString)));
}

void ServerApi::connectToServer(QString addr){
    _socket.open(QUrl(addr));
}

void ServerApi::closeConnection(){
    _socket.close();
}

bool ServerApi::login(QString &username, QString &password){
    //<commandCode>,<name>;
    QString message = QString("%0,%1").arg(SET_NAME).arg(username);
    int send = _socket.sendTextMessage(message);
    emit loggedIn(username);
    return send > 0;
}

void ServerApi::registerMatchmaking(){
    //<registerCommandCode>,<gameId>
    _socket.sendTextMessage(QString("%0,%1").arg(ADD_POOL_COMMAND).arg(TWO_CARS_ID));
    emit registeredMatchmaking();
}

void ServerApi::connected(){
    qDebug() << "Connected to server.";

    emit connectedSuccess();
}

void ServerApi::stateChanged(QAbstractSocket::SocketState state){
}

void ServerApi::error(QAbstractSocket::SocketError error){
    qDebug() << "Socket Error -> " << error;
    emit connectionError();
}

bool ServerApi::isConnected(){
    return _socket.state() == QAbstractSocket::ConnectedState;
}

void ServerApi::textMessageReceived(QString message){
    qDebug() << "Message =>" << message;
    Message msg(message);

    if(msg.code() == MATCH_FOUND_COMMAND){
        QString     serverAddr = msg.args().at(0);
        emit matchFound(serverAddr);
    }

}
