#ifndef SERVERAPI_H
#define SERVERAPI_H

#include <QObject>
#include <QWebSocket>

#define ADD_POOL_COMMAND    "1"
#define MATCH_FOUND_COMMAND "5"
#define SET_NAME            "3"
#define TWO_CARS_ID         "1"

class ServerApi : public QObject
{

    Q_OBJECT

public:

    ServerApi(QObject *parent = Q_NULLPTR);
    void    connectToServer(QString addr);
    void    closeConnection();
    bool    isConnected();
    void    registerMatchmaking();
    bool    login(QString &username , QString &password);

private slots:

    void    textMessageReceived(QString message);
    void    connected();
    void    stateChanged(QAbstractSocket::SocketState state);
    void    error(QAbstractSocket::SocketError error);

signals:

    void    connectedSuccess();
    void    connectionError();
    void    loggedIn(QString userName);
    void    registeredMatchmaking();
    void    matchFound(QString &serverAddr);

private:

    QWebSocket  _socket;

};

#endif // SERVERAPI_H
