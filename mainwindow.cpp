#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _api.connectToServer("ws://localhost:9000");
    connect(&_api , SIGNAL(connectedSuccess()) , this , SLOT(connected()));
    connect(&_api , SIGNAL(connectionError()) , this , SLOT(connectionError()));
    connect(&_api , SIGNAL(loggedIn(QString)), this , SLOT(loggedIn(QString)));
    connect(&_api , SIGNAL(registeredMatchmaking()) , this , SLOT(registerMatchmaking(bool)));
    connect(&_api , SIGNAL(matchFound(QString&)) , this , SLOT(matchFound(QString&)));

    connect(ui->loginButton , SIGNAL(clicked(bool)) , this , SLOT(login(bool)));
    connect(ui->registerMatchmakingButton , SIGNAL(clicked(bool)), this , SLOT(registerMatchmaking(bool)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::login(bool enabled){
    QString     password;
    QString     userName = ui->usernameTextBox->text().trimmed();
    _api.login(userName , password);
}

void MainWindow::registerMatchmaking(bool enabled){
    _api.registerMatchmaking();
}

void MainWindow::connected()    {
    ui->stateLabel->setText("Sunucuya baglanildi.");
}

void MainWindow::connectionError()  {
    ui->stateLabel->setText("Sunucu baglantisi saglanamadi.");
}

void MainWindow::loggedIn(QString userName) {
    ui->registerMatchmakingButton->setEnabled(true);
    ui->stateLabel->setText("Giris yapildi.");
}

void MainWindow::registeredMatchmaking(){
    ui->registerMatchmakingButton->setEnabled(false);
    ui->stateLabel->setText("Mac araniyor");
}

void MainWindow::matchFound(QString &serverUrl){
    qDebug() << "Mac bulundu. Url => " << serverUrl;
    QString userName = ui->usernameTextBox->text();
    _gameForm.show();
    _gameForm.setName(userName);
    _gameForm.connectGameServer(serverUrl);
}
