#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "serverapi.h"
#include "gameform.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:

    void    login(bool enabled);
    void    registerMatchmaking(bool enabled);
    void    connected();
    void    connectionError();
    void    loggedIn(QString userName);
    void    registeredMatchmaking();
    void    matchFound(QString  &serverUrl);

private:

    Ui::MainWindow  *ui;
    ServerApi       _api;
    GameForm        _gameForm;

};

#endif // MAINWINDOW_H
