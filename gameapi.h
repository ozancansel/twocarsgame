#ifndef GAMEAPI_H
#define GAMEAPI_H

#include <QWebSocket>
#include <QObject>
#include <QQueue>
#include "serverapi.h"

#define  WAITING_EVERYONE_TO_CONNECT_STATE      "0"
#define  LOBBY_NOT_READY_STATE                  "1"
#define  GAME_STARTING_STATE                    "2"
#define  GAME_STARTED_STATE                     "4"

//Incoming Commands
#define  PLAYER_INFORMATIONS_COMMAND            "12"
#define  GAME_STATE_CHANGED_COMMAND             "13"

//Outcoming Commands
#define  SET_READY_COMMAND                      "10"
#define  GAME_LOADED_COMMAND                    "11"


class GameApi : public QObject
{

    Q_OBJECT

public:

    GameApi(QObject *parent = Q_NULLPTR);
    enum    GameState{WAITING_EVERYONE_TO_CONNECT = 0,
                      LOBBY_NOT_READY = 1,
                      GAME_STARTING = 2,
                      GAME_STARTED = 3 };

    void        connectToGame(QString &url);
    bool        debugEnabled();
    void        setDebugEnabled(bool enabled);
    void        login(QString &name);
    void        timerEvent(QTimerEvent *event);

private slots:

    void        connected();
    void        textMessageReceived(QString message);
    void        error(QAbstractSocket::SocketError  error);

signals:

    void        connectedToGame();
    void        players(QStringList &players);
    void        stateChanged(GameState state , GameState previousState);
    void        loadGame();

private:

    QWebSocket      _socket;
    bool            _debugEnabled;
    GameState       _state;
    QQueue<QString> _commandQueue;
    QMap<QString , GameState>       _gameStateToStringMap;

    void            changeState(GameState state);

};

#endif // GAMEAPI_H
