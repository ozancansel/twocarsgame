#include "message.h"

Message::Message(QObject *parent) : QObject(parent)
{

}

Message::Message(QString &msg, QObject *parent) : QObject(parent){
    deserialize(msg);
}

void Message::setCode(QString &code){
    _code = code;
}

void Message::setArgs(QStringList args){
    _args = args;
}

QString Message::code(){
    return _code;
}

QStringList Message::args(){
    return _args;
}

void Message::deserialize(QString &message){
    QStringList l = message.split(",");
    QString code = l.at(0);
    l.removeAt(0);
    setCode(code);
    setArgs(l);
}

QString Message::serialize(){
    QString msg(_code);

    foreach (QString arg, _args) {
        msg.append(QString(",%0").arg(arg));
    }

    return msg;
}
