#include "gameform.h"
#include "ui_gameform.h"

GameForm::GameForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GameForm)
{
    ui->setupUi(this);
    opponent.nameLabel = ui->opponentName;
    opponent.readyLabe = ui->opponentReadyLabel;

    connect(&_api , SIGNAL(players(QStringList&)) , this , SLOT(players(QStringList&)));
}

GameForm::~GameForm()
{
    delete ui;
}

void GameForm::connectGameServer(QString &url){
    _api.connectToGame(url);
    _api.login(_userName);

}

void GameForm::setName(QString &name){
    _userName = name;
}

void GameForm::players(QStringList &players){
    foreach (QString name, players) {
        if(name != _userName){
            opponent.player = name;
            opponent.nameLabel->setText(opponent.player);
        }
    }
}
