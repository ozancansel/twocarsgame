#ifndef MESSAGEPARSER_H
#define MESSAGEPARSER_H

#include <QObject>
#include <QString>

class Message : public QObject
{

public:

    Message(QObject *parent = Q_NULLPTR);
    Message(QString &msg , QObject* parent = Q_NULLPTR);
    void        setCode(QString &code);
    void        setArgs(QStringList args);
    QString     code();
    QStringList args();
    void        deserialize(QString &message);
    QString     serialize();
private:

    QStringList _args;
    QString     _code;

};

#endif // MESSAGEPARSER_H
